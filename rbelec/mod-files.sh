echo "Modifying installation..."

echo -n "Modify /boot/config.txt "
echo "" | sudo tee -a /boot/config.txt
echo "# Added by raspboot" | sudo tee -a /boot/config.txt
echo "temp_limit=77" | sudo tee -a /boot/config.txt
echo "dtparam=pwr_led_trigger=heartbeat" | sudo tee -a /boot/config.txt
echo "dtparam=act_led_trigger=heartbeat" | sudo tee -a /boot/config.txt

echo "[OK]"


echo "Electricity monitor installation..."
git clone -b zappi https://gitlab.com/mausy5043-diagnostics/kamstrupd.git "${HOME}/kamstrupd"
# set permissions
# chmod -R 0755 "${HOME}/kamstrupd"
pushd "${HOME}/kamstrupd" || exit 1
  ./install.sh
  ./update.sh
popd || exit 1

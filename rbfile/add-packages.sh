install_package() {
  # See if packages are installed and install them.
  package=${1}
  echo "*********************************************************"
  echo "* Requesting ${package}"
  status=$(dpkg-query -W -f='${Status} ${Version}\n' "${package}" 2>/dev/null | wc -l)
  if [ "${status}" -eq 0 ]; then
    echo "* Installing ${package}"
    echo "*********************************************************"
    sudo apt-get -yuV install "${package}"
  else
    echo "* Already installed !!!"
    echo "*********************************************************"
  fi
}

echo
echo "Updating..."
sudo apt-get update
sudo apt-get -y autoclean
sudo apt-get -y autoremove
sudo apt-get -yuV upgrade

echo
echo "Add mountpoint for external HDD "
sudo mkdir -p /mnt/icybox
echo "# Mountpoint for external HDD " | sudo tee -a /etc/fstab
echo "/dev/sda1        /mnt/icybox    ext4    defaults    0    2" | sudo tee -a /etc/fstab
sudo mount /mnt/icybox
# wait for the disk to get mounted properly
sleep 60
sync
sync
echo "Add bind-mountpoints for NFS-fileserver "
sudo mkdir -p /srv/nfs/config
sudo mkdir -p /srv/nfs/databases
sudo mkdir -p /srv/nfs/files
sudo chown pi:users /srv/nfs/config
sudo chown pi:users /srv/nfs/databases
sudo chown pi:users /srv/nfs/files
echo "# Exportpoints for NFS-fileserver " | sudo tee -a /etc/fstab
echo "/mnt/icybox/config        /srv/nfs/config       none    bind    0    0" | sudo tee -a /etc/fstab
echo "/mnt/icybox/databases     /srv/nfs/databases    none    bind    0    0" | sudo tee -a /etc/fstab
echo "/mnt/icybox/files         /srv/nfs/files        none    bind    0    0" | sudo tee -a /etc/fstab
sudo mount /srv/nfs/config
sudo mount /srv/nfs/databases
sudo mount /srv/nfs/files
echo

install_package "graphviz"
install_package "smartmontools"
install_package "nut-client"
# install_package() does not work for nfs-kernel-server
sudo apt-get -yuV install "nfs-kernel-server"
sudo apt-get -yuV install "sqlite3"

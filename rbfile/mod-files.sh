echo "Set permissions... [nut]"
sudo chmod 0640 /etc/nut/*

echo "Modify /boot/config.txt "
echo "" | sudo tee -a /boot/config.txt
echo "# Added by raspboot" | sudo tee -a /boot/config.txt
echo "temp_limit=77" | sudo tee -a /boot/config.txt
echo "dtparam=pwr_led_trigger=heartbeat" | sudo tee -a /boot/config.txt
echo "dtparam=act_led_trigger=heartbeat" | sudo tee -a /boot/config.txt
echo "[OK]"
echo

#! /bin/bash

# This script only gets executed if `/tmp/raspboot.reboot` is absent...
# /tmp is in RAMFS, so everytime the server is rebooted, this script is executed.

CLNT=$(hostname)
ME=$(whoami)

echo -n "post-boot script started on: "
date

# /var/log is on tmpfs so recreate lastlog now
if [ ! -e /var/log/lastlog ]; then
  sudo touch /var/log/lastlog
  sudo chgrp utmp /var/log/lastlog
  sudo chmod 664 /var/log/lastlog
fi

# Additional scripts to be executed on the first boot after install.
# This makes the `raspbian-ua-netinst` installer more uniform and easier
# to maintain regardless of the use.
if [ ! -e "${HOME}/.firstboot" ]; then
  echo -n "First boot detected on "
  date

  # 1. Update the system
  echo
  echo "Installing dotfiles..."
  . "${HOME}/dotfiles/install_pi.sh"
  ln -s "${HOME}/.bin" "${HOME}/bin"
  cp /srv/config/.netrc "${HOME}/.netrc"
  chmod 0600 "${HOME}/.netrc"
  mkdir -p "${HOME}/.config"
  cp /srv/config/.rsync.secret "${HOME}/.config/.rsync.secret"
  chmod 0600 "${HOME}/.config/.rsync.secret"
  cp /srv/config/.mailrc "${HOME}/.mailrc"
  chmod 0600 "${HOME}/.mailrc"
  echo
  echo "Updating..."

  sudo apt-get update
  # Install server specific-packages
  echo
  echo "Additional packages installation..."
  if [ -e "./${CLNT}/add-packages.sh" ]; then
    source "./${CLNT}/add-packages.sh"
  fi

  # Install server specific configuration files
  echo
  echo "Copy configuration files..."
  for f in ./"${CLNT}"/config/*; do
    # g=$(echo "$(basename $f)" | sed 's/@/\//g')
    g=$(basename "${f}" | sed 's/@/\//g')
    echo "${f} --> ${g}"
    # path must already exist for this to work:
    sudo cp "${f}" "/${g}"
  done

  # Modify existing server specific configuration files
  echo
  echo "Modify installation..."
  if [ -e "./${CLNT}/mod-files.sh" ]; then
    source "./${CLNT}/mod-files.sh"
  fi

  # Plant the flag and wrap up
  if [ -e /bin/journalctl ]; then
    sudo usermod -a -G systemd-journal "${ME}"
  fi
  touch "${HOME}/.firstboot"

  # The next line is used to run a test on the hardware RNG.
  # date; sudo cat /dev/random | rngtest -c 5000 | logger -t rngtest; date
  # The next line is used to run a speedtest of openSSL.
  # date; openssl speed | logger -t openssl-speed; date

  sudo shutdown -r +1 "First boot installation completed. Please log off now."
  echo -n "First boot installation completed on "
  date
fi

echo "Boot detection mail... $(date)"
python3 "${HOME}/bin/pymail.py" --subject "${CLNT} was booted on $(date)"

echo "Modifying installation..."

echo -n "Modify /boot/config.txt "
echo "" | sudo tee -a /boot/config.txt
echo "# Added by raspboot" | sudo tee -a /boot/config.txt
echo "dtoverlay=lirc-rpi,gpio_in_pin=17,gpio_out_pin=22" | sudo tee -a /boot/config.txt
echo "dtparam=pwr_led_trigger=heartbeat" | sudo tee -a /boot/config.txt
echo "dtparam=act_led_trigger=heartbeat" | sudo tee -a /boot/config.txt

echo "[OK]"

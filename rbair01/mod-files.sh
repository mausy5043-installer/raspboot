echo "Modifying installation..."

echo -n "Modify /boot/config.txt "
echo "" | sudo tee -a /boot/config.txt
echo "# Added by raspboot" | sudo tee -a /boot/config.txt
echo "dtoverlay=lirc-rpi,gpio_in_pin=17,gpio_out_pin=22" | sudo tee -a /boot/config.txt
# LED control
echo "dtparam=pwr_led_trigger=heartbeat" | sudo tee -a /boot/config.txt
echo "dtparam=act_led_trigger=heartbeat" | sudo tee -a /boot/config.txt
# Bluetooth enable
sudo sed -i 's/dtoverlay=disable-bt/\#dtoverlay=disable-bt/g' /boot/config.txt
# I2C enable
sudo sed -i 's/\#dtparam=i2c_arm=o.*$/dtparam=i2c_arm=on/g' /boot/config.txt
# Bluetooth enable
sudo sed -i 's/\#dtparam=spi=o.*$/dtparam=spi=on/g' /boot/config.txt

echo "[OK]"

echo -n "Change LED control after boot"
echo "@reboot           root    echo cpu >  /sys/class/leds/led0/trigger" | sudo tee -a /etc/cron.d/99leds
echo "@reboot           root    echo mmc1 > /sys/class/leds/led1/trigger" | sudo tee -a /etc/cron.d/99leds

# switch off annoying eth0
echo "@reboot           root    sleep 120; systemctl stop ifup@eth0.service" | sudo tee -a /etc/cron.d/98no-eth0

echo "[OK]"

echo "Installing AIRCON package..."
git clone https://gitlab.com/mausy5043-raspberrypi-io/aircon.git "${HOME}/aircon"
pushd "${HOME}/aircon" || exit 1
  ./install.sh
  ./update.sh
popd || exit 1

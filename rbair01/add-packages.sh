install_package() {
  # See if packages are installed and install them.
  package=${1}
  echo "*********************************************************"
  echo "* Requesting ${package}"
  status=$(dpkg-query -W -f='${Status} ${Version}\n' "${package}" 2>/dev/null | wc -l)
  if [ "${status}" -eq 0 ]; then
    echo "* Installing ${package}"
    echo "*********************************************************"
    sudo apt-get -yuV install "${package}"
  else
    echo "* Already installed !!!"
    echo "*********************************************************"
  fi
}

echo
echo "Updating..."
sudo apt-get update
sudo apt-get -y autoclean
sudo apt-get -y autoremove
sudo apt-get -yuV upgrade

echo "Installing WIFI support..."
install_package "wavemon"
install_package "usbutils"

echo "Installing Bluetooth support..."
install_package "pi-bluetooth"

# FOR TESTING PURPOSES ONLY!
# In future Python packages will be installed by the relevant repo that requires them
# GitHub repos are installed here only for testing during development
echo
echo "Installing additional packages..."
install_package "graphviz"
#install_package "build-essential"
#install_package "i2c-tools"
#install_package "spi-tools"
#sudo addgroup --gid 114 spi
#sudo usermod -aG spi,i2c,plugdev pi
#sudo chown root:spi /dev/spidev0.*
#
## Support for matplotlib & numpy
#install_package "libatlas-base-dev"
## Support for Luma.OLED
#install_package "libfreetype6-dev"
#install_package "libjpeg-dev"
#install_package "libopenjp2-7"
#install_package "libtiff5"
##install_package "lirc" disable until USB-key is available
#install_package "python3-dev"
#install_package "python3-pip"
#install_package "python3-rpi.gpio"
#install_package "python3-smbus"
#install_package "sqlite3"
#install_package "wiringpi"
#gpio readall

# FOR TESTING PURPOSES ONLY!
# In future Python packages will be installed by the relevant repo that requires them
# GitHub repos are installed here only for testing during development
# sudo -H
#echo
#echo "Installing required Python packages"
#python3 -m pip install --upgrade pip setuptools wheel
#python3 -m pip install -r "${HOME}/raspboot/rbair11/requirements.txt"
#
#echo
#echo "Installing GitHub repos"
#pushd "${HOME}" || exit 1
#  git clone https://github.com/nanomesher/Nanomesher_ProdBoard.git
#  git clone https://github.com/rm-hull/luma.examples.git
#
#  git clone https://github.com/adafruit/Adafruit_Python_BMP.git
#
#  git clone https://github.com/adafruit/Adafruit_CCS811_python
#  git clone https://github.com/adafruit/Adafruit_CircuitPython_CCS811
#
#  git clone https://github.com/ralf1070/Adafruit_Python_SHT31.git
#  git clone https://github.com/adafruit/Adafruit_SHT31.git
#  git clone https://github.com/adafruit/Adafruit_CircuitPython_SHT31D
#popd || exit 1

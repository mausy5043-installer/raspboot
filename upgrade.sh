#!/bin/bash

# Set this to the current release you are on
thisrelease=buster
# Set this to the release you wish to migrate to
nextrelease=bullseye

export DEBIAN_FRONTEND=noninteractive
export UCF_FORCE_CONFFOLD=1

# make sure we are current.
date
echo "**********************************"
echo "* Preparing change-over          *"
echo "**********************************"
/home/pi/dotfiles/pios/bin/sysupdate.sh

# Checks are not needed on a fresh system

#echo
#echo "**********************************"
#echo "* Consistency Check              *"
#echo "**********************************"
## source: https://linuxconfig.org/raspbian-gnu-linux-upgrade-from-jessie-to-raspbian-stretch-9
#DPKGC=$(sudo dpkg -C | wc -l)
#if [ $DPKGC -ne 0 ]; then
#  sudo dpkg -C
#  echo
#  echo "*** Consistency check returned a list of partially installed, missing and/or obsolete packages."
#  echo "*** Repair these issues before continuing."
#  exit 1
#fi
#
#echo
#echo "**********************************"
#echo "* HOLDs Check                    *"
#echo "**********************************"
#APTHOLDS=$(sudo apt-mark showhold | wc -l)
#if [ $APTHOLDS -ne 0 ]; then
#  sudo apt-mark showhold
#  echo
#  echo "*** There are packages ON HOLD."
#  echo "*** Repair these issues before continuing."
#  exit 1
#fi

echo
echo "**********************************"
echo "* Change release in config-files *"
echo "**********************************"
sudo sed -i "s/${thisrelease}/${nextrelease}/g" /etc/apt/sources.list
cat /etc/apt/sources.list
echo
sudo sed -i "s/${thisrelease}/${nextrelease}/g" /etc/apt/sources.list.d/raspberrypi.org.list
cat /etc/apt/sources.list.d/raspberrypi.org.list
echo

echo
echo "***"
echo "*** UPGRADING FROM ${thisrelease} TO ${nextrelease}"
echo "*** THIS WILL TAKE A FEW MINUTES"
echo "***"

echo
date
echo "**********************************"
echo "* Executing upgrade              *"
echo "**********************************"
/home/pi/dotfiles/pios/bin/sysupdate.sh

echo
date
echo "**********************************"
echo "* Executing post-upgrade cleanup *"
echo "**********************************"
/home/pi/dotfiles/pios/bin/sysupdate.sh

echo
date
echo "**********************************"
echo "* Change-over COMPLETE           *"
echo "**********************************"
echo "Rebooting to activate the new system."
sudo systemctl reboot

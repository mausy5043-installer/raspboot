#!/bin/bash

do_query() {
    command="${1}"
    sudo mysql --defaults-file="${config}" -u root -h localhost -e "${command}"
    return $?
}

echo "Modifying MariaDB installation.."
esc_pass="$(grep password $config |awk -F\" '{print $2}')"

echo " - Set root password"
 do_query "UPDATE mysql.user SET plugin = 'mysql_native_password', Password = PASSWORD('$esc_pass') WHERE User='root';"
 do_query "FLUSH PRIVILEGES;"
echo " - Remove anonymous users"
 do_query "DELETE FROM mysql.user WHERE User='';"
echo " - Remove remote root"
 do_query "DELETE FROM mysql.user WHERE User='root' AND Host NOT IN ('localhost', '127.0.0.1', '::1');"
echo " - Drop test database"
 do_query "DROP DATABASE IF EXISTS test;"
 do_query "DELETE FROM mysql.db WHERE Db='test' OR Db='test\\_%'"
echo " - Reload privileges"
 do_query "FLUSH PRIVILEGES;"

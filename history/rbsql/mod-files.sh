#!/bin/bash

config="$HOME/.my.cnf"

getfilefromserver() {
file="${1}"
mode="${2}"

  cp -rvf  "$HOME/bin/.config/home/${file}" "$HOME/"
  chmod    "${mode}" "$HOME/${file}"
}

do_query() {
    command="${1}"
    sudo mysql --defaults-file="${config}" -u root -h localhost -e "${command}"
    return $?
}

echo "Modifying installation..."

echo -n "Modify /boot/config.txt "
# Scaled CPU frequency
echo ""                         | sudo tee -a /boot/config.txt
echo "# Added by raspboot"      | sudo tee -a /boot/config.txt
echo "arm_freq=950"             | sudo tee -a /boot/config.txt
echo "core_freq=450"            | sudo tee -a /boot/config.txt
echo "sdram_freq=450"           | sudo tee -a /boot/config.txt
echo "over_voltage=6"           | sudo tee -a /boot/config.txt
echo ""                         | sudo tee -a /boot/config.txt
echo "arm_freq_min=500"         | sudo tee -a /boot/config.txt
echo "core_freq_min=250"        | sudo tee -a /boot/config.txt
echo "sdram_freq_min=400"       | sudo tee -a /boot/config.txt
echo "over_voltage_min=0"       | sudo tee -a /boot/config.txt
echo ""                         | sudo tee -a /boot/config.txt
echo "force_turbo=0"            | sudo tee -a /boot/config.txt
echo "temp_limit=70"            | sudo tee -a /boot/config.txt

echo "[OK]"

echo "Copying files from server.."
getfilefromserver ".my.sql.cnf" "0740"
mv "$HOME/.my.sql.cnf" "$config"

echo "Modifying MariaDB installation.."
esc_pass="$(grep password $config |awk -F\" '{print $2}')"

echo " - Set root password"
 do_query "UPDATE mysql.user SET plugin = 'mysql_native_password', Password = PASSWORD('$esc_pass') WHERE User='root';"
 do_query "FLUSH PRIVILEGES;"
echo " - Remove anonymous users"
 do_query "DELETE FROM mysql.user WHERE User='';"
echo " - Remove remote root"
 do_query "DELETE FROM mysql.user WHERE User='root' AND Host NOT IN ('localhost', '127.0.0.1', '::1');"
echo " - Drop test database"
 do_query "DROP DATABASE IF EXISTS test;"
 do_query "DELETE FROM mysql.db WHERE Db='test' OR Db='test\\_%'"
echo " - Reload privileges"
 do_query "FLUSH PRIVILEGES;"

install_package()
{
  # See if packages are installed and install them.
  package=$1
  echo "*********************************************************"
  echo "* Requesting $package"
  status=$(dpkg-query -W -f='${Status} ${Version}\n' "$package" 2>/dev/null | wc -l)
  if [ "$status" -eq 0 ]; then
    echo "* Installing $package"
    echo "*********************************************************"
    sudo apt-get -yuV install "$package"
  else
    echo "* Already installed !!!"
    echo "*********************************************************"
  fi
}

RMTSERVER="asgard"

#echo "${RMTSERVER}:/srv/array1/datastore/rbsql /var/lib/mysql   nfs4   nouser,atime,rw,dev,exec,suid,intr,_netdev,x-systemd.automount,noauto  0   0"  | sudo tee --append /etc/fstab
#sudo mount /var/lib/mysql

echo "Installing additional packages..."
install_package "mariadb-server"
install_package "mytop"

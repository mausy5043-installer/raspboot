install_package() {
  # See if packages are installed and install them.
  package=${1}
  echo "*********************************************************"
  echo "* Requesting ${package}"
  status=$(dpkg-query -W -f='${Status} ${Version}\n' "${package}" 2>/dev/null | wc -l)
  if [ "${status}" -eq 0 ]; then
    echo "* Installing ${package}"
    echo "*********************************************************"
    sudo apt-get -yuV install "${package}"
  else
    echo "* Already installed !!!"
    echo "*********************************************************"
  fi
}

echo
echo "Updating..."
sudo apt-get update
sudo apt-get -y autoclean
sudo apt-get -y autoremove
sudo apt-get -yuV upgrade

echo
echo "Add mountpoint for external HDD "
sudo mkdir -p /mnt/icybox
echo "/dev/sda1 /mnt/icybox ext4 defaults  0   2" | sudo tee -a /etc/fstab
sudo mount /mnt/icybox
echo

install_package "graphviz"
install_package "smartmontools"
# install_package "nut-client"

# wait for the disk to get mounted properly
sleep 60
sync
sync

# install docker after mounting the external disk because it holds the configuration files.
echo
echo "Installing Docker"
curl -sSL https://get.docker.com | sh
sudo usermod -aG docker pi

#! /bin/bash

CLNT=$(hostname)
ME=$(whoami)
source /etc/os-release

# Timestamp the logfile
date

# Change PWD to the binaries directory
pushd "${HOME}/raspboot" || exit 1
  # Re-entrancy prevention
  if [ ! -e /tmp/raspboot.reboot ]; then
    whoami > /tmp/raspboot.reboot

#    # Check for current release
#    if [ "$VERSION_ID" == "10" ]; then
#      echo "Debian 10; UPGRADE TO Debian 11"
#      # upgrade and reboot here
#      ./upgrade.sh
#    else
#      echo "Not Debian 10. Continuing..."
#    fi
    git config --global core.fileMode false
    git config --global pull.rebase false
    ( ./01-post-boot.sh 2>&1 | tee -a ../post-boot.log | logger -p local7.info -t 01-post-boot ) &

    systemd-analyze | logger -t analyzer1
    systemd-analyze critical-chain | logger -t analyzer2

    (sleep 3660; systemd-analyze critical-chain --fuzz=1h | logger -t late-analyzer) &
  fi
popd || exit

